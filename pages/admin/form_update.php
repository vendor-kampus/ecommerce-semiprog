<?php if($page=="update_barang") : ?>
	<?php 
		//sama seperti tampilan detail, untuk membuat form update 
		// diperlukan 1 data dari student 
		include '../../class/Barang.php';
		$barang = new Barang();
		$data = null;
		if(isset($_GET['bar_id'])){
			//mengambil semua data berdasarkan nrp 
			$data = $barang->getDetail($_GET['bar_id']);
		}
	?>
	<p> 
		<h5> Ubah Data Barang : <?= $_GET['bar_id']; ?> </h5>
		<form method="post" action="../../controllers/barang/update.php"  enctype="multipart/form-data">
			<div class="form-group">
<!--
			 NRP di sembunyikan, karena primary key tidak boleh di ubah 
				ganti semua placeholder menjadi value 	
--> 
			
			<input type="hidden" class="form-control" name="bar_id" value="<?= $data['bar_id'] ?>" >
			</div>
			<!-- <select name="nama" id="select" class="form-control">
			<?php 
                //include "../../class/Student.php";
                //$student = new Student();
            ?>
                <?php //foreach ($student->getData() as $data): ?>
                  <option value="<?= $data['nama']; ?>"><?= $data['nama']; ?></option>        
                <?php //endforeach ?> 
             </select> -->
             
            <div class="form-group">
				<label for="nama"> Nama Barang </label>
				<input type="text" class="form-control" name="bar_nama" value="<?= $data['bar_nama'] ?>">
			</div>

            <div class="form-group">
				<label for="nama"> Harga Jual </label>
				<input type="text" class="form-control" name="bar_hargaJual" value="<?= $data['bar_hargaJual'] ?>">
			</div>

            <div class="form-group">
				<label for="nama"> Jenis </label>
				<input type="text" class="form-control" name="bar_jenis" value="<?= $data['bar_jenis'] ?>">
			</div>
			<div class="form-group">
				<label for="nama"> Foto </label>
				<input type="file" class="form-control" name="upload_foto" value="">
			</div>



			
			<button type="submit" class="btn btn-success mb-2"> Ubah </button>
			<button type="reset" class="btn btn-success mb-2"> Reset </button>


		</form> 
<?php endif; ?>