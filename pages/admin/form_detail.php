<?php if($page=="detail_barang") : ?>
	<?php 
		//sama seperti tampilan detail, untuk membuat form update 
		// diperlukan 1 data dari student 
	include '../../class/Barang.php';
	$barang = new Barang();
	$data = null;
	if(isset($_GET['bar_id'])){
			//mengambil semua data berdasarkan nrp 
		$data = $barang->getDetail($_GET['bar_id']);
	}
	?>

	<p>
		<div class="row">
			<div class="col-md-9">
				<h3> Detail Barang </h3>
			</div>		
			<div class="col-md-3">
				<a href="index.php?page=tabel_barang" class="btn btn-success"> Kembali </a> 
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<center>
					<img src="../../res/foto_produk/<?= $data['bar_foto'] ?>" style="width: 200px; height: 250px;" />
				</center>
			</div>
		</div>


	<!-- <a href="index.php?page=student-create" class="btn btn-success"> Tambah </a>
		<a href="index.php?page=student" class="btn btn-success"> Kembali </a> -->
	</p>
	<br>
	<P>
		<?php if($data) : ?>
			<table class="table table-hover table-dark" >
				<tr>
					<th class="text-right"> Id Barang </th>
					<td> <?= $data['bar_id'] ?> </td>
				</tr>
				<tr>
					<th class="text-right"> Nama </th>
					<td> <?= $data['bar_nama'] ?> </td>
				</tr>
				<tr>
					<th class="text-right"> Jenis </th>
					<td> <?= $data['bar_jenis'] ?> </td>
				</tr>
				<tr>
					<th class="text-right"> Harga Jual </th>
					<td>Rp. <?= number_format($data['bar_hargaJual']) ?> </td>
				</tr>
			</table>
		<?php endif; ?>
	</p>
	<?php endif; ?>