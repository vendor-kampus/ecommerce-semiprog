<?php
if(!isset($_SESSION))
{
	session_start();
}
include "class/Barang.php";
$barang = new Barang();

$subtotal=0;
$subharga=0;

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>E-Commerce</title>
	<link rel="stylesheet" href="Style/tambahan/css/bootstrap.min.css"/>
	<script src="Style/tambahan/js/jquery2.js"></script>
	<script src="Style/tambahan/js/bootstrap.min.js"></script>
	<!-- <script src="Style/tambahan/main.js"></script> -->
	<link rel="stylesheet" type="text/css" href="Style/tambahan/style.css">
	<style></style>
</head>
<?php
//cek halaman yang di tuju
array_key_exists('page', $_GET) ? $page = $_GET['page'] : $page = '';
?>
<body>
	<div class="wait overlay">
		<div class="loader"></div>
	</div>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse" aria-expanded="false">
					<span class="sr-only">navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="#" class="navbar-brand">E-Commerce</a>
			</div>
			<div class="collapse navbar-collapse" id="collapse">
				<ul class="nav navbar-nav">
					<!-- <li><a href="index.php"><span class="glyphicon glyphicon-home"></span>Home</a></li> -->
					<li><a href="index-tran.php"><span class="glyphicon glyphicon-modal-window"></span>Product</a></li>
				</ul>
			</div>
		</div>
	</div>
	<p><br/></p>
	<p><br/></p>
	<p><br/></p>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8" id="cart_msg">
				<!--Cart Message-->
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="panel panel-primary">
					<div class="panel-heading">Cart Checkout</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-1 col-xs-2"><b>Action</b></div>
							<div class="col-md-2 col-xs-2"><b>Product Image</b></div>
							<div class="col-md-2 col-xs-2"><b>Product Name</b></div>
							<div class="col-md-2 col-xs-2"><b>Quantity</b></div>
							<div class="col-md-2 col-xs-2"><b>Product Price</b></div>
							<div class="col-md-2 col-xs-2"><b>Price in Rp</b></div>
							<div class="col-md-1 col-xs-2"><b>Ukuran</b></div>

						</div>
						<!-- <div id="cart_checkout"></div> -->
						<?php foreach ($_SESSION['keranjang'] as $id_barang => $jumlah): ?>
							<?php
							$dataBarang = $barang->getDetail($id_barang);
							$subharga = (int) $dataBarang['bar_hargaJual']*$jumlah;
							$subtotal = $subtotal + $subharga;
							?>
							<div class="row">
								<div class="col-md-1">
									<div class="btn-group">
										<a href="detail-tran.php?page=delete_cart&id_barang=<?= $dataBarang['bar_id'] ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
										<!-- index.php?page=penjualan-delete&id_barang -->
										<!-- <a href="" class="btn btn-primary"><span class="glyphicon glyphicon-ok-sign"></span></a> -->
									</div>
								</div>
								<div class="col-md-2"><img src='res/foto_produk/<?= $dataBarang['bar_foto'] ?>' width="50" height="40"/></div>
								<div class="col-md-2"><?= $dataBarang['bar_nama'] ?> </div>
								<div class="col-md-2"><input type='text' class='form-control' value='<?= $jumlah ?>' readonly></div>
								<div class="col-md-2"><input type='text' class='form-control' value='<?= number_format($dataBarang['bar_hargaJual']) ?>' readonly></div>
								<div class="col-md-2"><input type='text' class='form-control' value='<?= number_format($subharga) ?>' readonly></div>
								

								<?php 
									if($dataBarang['bar_jenis']=="Baju"){
								?>
									<div class="col-md-1"><input type='text' name="bar_id" class='form-control' value='<?= $_SESSION['keterangan'][$id_barang] ?>' readonly></div>
							<?php } ?>
							</div>
							<br>
						<?php endforeach; ?>

						<br>
						<div class="row">
							<div class="col-md-8"></div>
							<div class="col-md-2">
								<b> Grand Total</b>
							</div>
							<div class="col-md-2">
								<b>Rp. <?= number_format($subtotal) ?></b>
							</div>
						</div>
					</div>
					<div class="panel-footer" >
						<div class="row">
							<div class="col-md-10">

							</div>
							<div class="col-md-2">
								<a href="detail-tran.php?page=checkout" class="btn btn-primary">Checkout</a>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
			<?php
			if($page=="delete_cart"){
				include "controllers/penjualan/delete_cart.php";
			}
			if($page=="checkout"){
				include "pages/admin/create_pelanggan.php";
			}


			?>
		</div>
	</body>
	</html>
<?php
// echo "<pre>";
// print_r($_SESSION);
// echo "</pre>";
?>