-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2019 at 05:51 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ecommerce`
--
CREATE DATABASE IF NOT EXISTS `db_ecommerce` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_ecommerce`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `bar_id` int(11) NOT NULL,
  `bar_nama` varchar(255) NOT NULL,
  `bar_hargaJual` char(255) NOT NULL,
  `bar_jenis` varchar(255) NOT NULL,
  `bar_foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`bar_id`, `bar_nama`, `bar_hargaJual`, `bar_jenis`, `bar_foto`) VALUES
(1, 'Peace Be Upon You Pink', '130000', 'Baju', 'Baju 1.jpg'),
(2, 'Salam Hijau', '130000', 'Baju', 'Baju 2.jpg'),
(3, 'Peace Be Upon You Blue', '120000', 'Baju', 'Baju 3.jpg'),
(4, 'Stop Wars', '150000', 'Baju', 'Baju 4.jpg'),
(5, 'Buku 1', '40000', 'Buku', 'Buku 1.jpg'),
(6, 'Buku 2', '45000', 'Buku', 'Buku 2.jpg'),
(7, 'Buku 3', '45000', 'Buku', 'Buku 3.jpg'),
(8, 'Buku 6', '70000', 'Buku ', 'Buku 6_11102019051629_Buku 4.jpg'),
(9, 'Buku 5', '50000', 'Buku', 'Buku 5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `detail_penjualan`
--

CREATE TABLE `detail_penjualan` (
  `penj_noTransaksi` varchar(255) NOT NULL,
  `bar_id` varchar(255) NOT NULL,
  `jumlah_beli` char(255) NOT NULL,
  `bar_hargaJual` char(255) NOT NULL,
  `subtotal_harga` char(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_penjualan`
--

INSERT INTO `detail_penjualan` (`penj_noTransaksi`, `bar_id`, `jumlah_beli`, `bar_hargaJual`, `subtotal_harga`, `keterangan`) VALUES
('20191', '1', '2', '130000', '260000', 'S'),
('20191', '2', '1', '130000', '130000', 'S'),
('20191', '4', '2', '150000', '300000', 'S'),
('20191', '7', '1', '45000', '45000', '-');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `adm_id` int(11) NOT NULL,
  `adm_nama` varchar(255) NOT NULL,
  `adm_email` varchar(255) NOT NULL,
  `adm_username` varchar(255) NOT NULL,
  `adm_password` varchar(255) NOT NULL,
  `adm_foto` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`adm_id`, `adm_nama`, `adm_email`, `adm_username`, `adm_password`, `adm_foto`, `created_at`, `update_at`) VALUES
(1, 'Hasan', 'hasan@gmail.com', 'admin', 'admin', 'yesaya_profile.jpg', '2019-09-20 18:21:39', '2019-10-11 00:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `penj_noTransaksi` varchar(255) NOT NULL,
  `pel_nama` varchar(255) NOT NULL,
  `pel_alamat` text NOT NULL,
  `pel_noHp` varchar(255) NOT NULL,
  `pel_jenisKelamin` varchar(255) NOT NULL,
  `pel_kota` varchar(255) NOT NULL,
  `penj_totalBayar` char(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`penj_noTransaksi`, `pel_nama`, `pel_alamat`, `pel_noHp`, `pel_jenisKelamin`, `pel_kota`, `penj_totalBayar`, `created_at`) VALUES
('20191', 'bagas', 'Jl. Jakarta ', '081123123123', 'Laki-Laki', 'Kab Bandung', '735000', '2019-10-10 23:18:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`bar_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`adm_id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`penj_noTransaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `bar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `adm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
