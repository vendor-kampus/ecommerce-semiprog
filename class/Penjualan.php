<?php
if(!class_exists('database')){
    require('database.php');
}

class Penjualan{
    public $penj_noTransaksi;
    public $pel_nama;
    public $pel_alamat;
    public $pel_jenisKelamin;
    public $pel_kota;
    public $penj_totalBayar;
    public $create_at;

    

     public function getData(){
      $db = new Database();
      $dbConnect = $db->connect();
      $sql = "SELECT * FROM penjualan";
      $data = $dbConnect->query($sql);
      $dbConnect = $db->close();
      return $data;
    }
    
    public function create() {
		$db = new Database();
			//membuka koneksi
		$dbConnect = $db->connect();

			//query menyimpan data
		$sql = "INSERT INTO Penjualan
		(
		penj_noTransaksi,
		pel_nama,
		pel_alamat,
		pel_jenisKelamin,
		pel_kota,
		penj_totalBayar
		)
		VALUES
		(
		'{$this->penj_noTransaksi}',
		'{$this->pel_nama}',
		'{$this->pel_alamat}',
		'{$this->pel_jenisKelamin}',
		'{$this->pel_kota}',
		'{$this->penj_totalBayar}'
	    )";
				//eksekusi query di atas
		$data = $dbConnect->query($sql);

				//menampung error simpan data
		$error = $dbConnect->error;

				//menutup koneksi
		$dbConnect = $db->close();

				//mengembalikan nilai error
		return $error;

	  }
	  




}

?>
